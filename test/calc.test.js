const expect = require('chai').expect;
const {calc} = require('../src/calc');

const x = 15;
const y = 3;

describe("calc", () => {
    it("Should add 2 numbers", () => {
        expect(calc("+",x,y)).to.equal(18);
    });
    it("Should substract one number from the other", () => {
        expect(calc("-", x,y)).to.equal(12);
    });
    it("Should multiply two numbers", () => {
        expect(calc("*",x,y)).to.equal(45);
    });
    it("Should divide two numbers", () => {
        expect(calc("/",x,y)).to.equal(5);
    });
    it("handles empty answers", () => {
        expect(calc(x,y)).to.contain('Error!');
    });
})