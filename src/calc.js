function calc (operator, num1, num2){
    if(!num1 || !num2){
        return "Error!";
    }else if(operator === "+"){
        return num1 + num2;
    }else if(operator === "-"){
        return num1-num2;
    }else if(operator === '*'){
        return num1*num2;
    }else if(operator === "/"){
        return num1/num2;
    }

}

exports.calc = calc;